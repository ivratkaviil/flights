'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var FlightsSchema = new Schema({

    airline: {type: String, required: true},
    departureCity: {type: String, required: true},
    capacity: {type: Number, required: true}

});

module.exports = mongoose.model('Flight', FlightsSchema);