    express = require('express');
    app = express();
    port = process.env.PORT || 4200;
    mongoose = require('mongoose');
    bodyParser = require('body-parser');
    Flight = require('./models/Flight'); // get our mongoose model
    /*User = require('./models/user'), // get our mongoose model
    routes = require('./routes/routes'),
    jwt = require('jsonwebtoken'), // used to create, sign, and verify tokens
    config = require('./config'), // get our config file
    User = require('./models/user'), // get our mongoose model
   */
    jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
    config = require('./config'); // get our config file
    User = require('./models/user'); // get our mongoose model


// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/Flights');


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var routes = require('./routes/routes'); //importing route
routes(app); //register the route


app.listen(port);


console.log('todo list RESTful API server started on: ' + port);





/*
// =======================
// get the packages we need ============
// =======================
express = require('express');
app = express();
bodyParser = require('body-parser');
morgan = require('morgan');
mongoose = require('mongoose');
routes = require('./routes/routes');
jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
config = require('./config'); // get our config file
User = require('./models/user'); // get our mongoose model
Flights = require('./models/Flights'); // get our mongoose model

routes(app);

// =======================
// configuration =========
// =======================
var port = process.env.PORT || 4200; // used to create, sign, and verify tokens
mongoose.connect(config.database); // connect to database
app.set('superSecret', config.secret); // secret variable

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// use morgan to log requests to the console
app.use(morgan('dev'));

// =======================
// routes ================
// =======================
// basic route
app.get('/', function (req, res) {
    res.send('Hello! The API is at http://localhost:' + port + '/api');
});

// API ROUTES -------------------
// we'll get to these in a second

app.get('/setup', function (req, res) {

    // create a sample user
    var tarvi = new User({
        name: 'Tarvi',
        password: 'muusika',
        admin: true
    });

    // save the sample user
    tarvi.save(function (err) {
        if (err) throw err;

        console.log('User saved successfully');
        res.json({success: true});
    });
});

var apiRoutes = express.Router();


apiRoutes.get('/', function (req, res) {
    res.json({message: 'Welcome to the coolest API on earth!'});
});

// route to return all users (GET http://localhost:8080/users)
apiRoutes.get('/users', function (req, res) {
    User.find({}, function (err, users) {
        res.json(users);
    });
});


// apply the routes to our application with the prefix /api
app.use('/api', apiRoutes);
// =======================
// start the server ======
// =======================
app.listen(port);
console.log('Magic happens at http://localhost:' + port);

*/
