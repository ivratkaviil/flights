'use strict';
module.exports = function (app) {
    var Flights = require('../controllers/Flights');
    var Users = require('../controllers/User');

    app.route('/login')
        .post(Users.find);

    app.route('/users/:id')
        .get(Users.find)

    app.route('/flights')
        .get(Flights.all)
        .post(Flights.create);


    app.route('/flights/:id')
        .get(Flights.find)
        .patch(Flights.update)
        .delete(Flights.delete);
};

