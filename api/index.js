const express = require('express');
const parseurl = require('parseurl');
const bodyParser = require('body-parser');
const session = require('express-session');
const mustacheExpress = require('mustache-express');
const data = require('./userData');
const data2 = require('./data');
const app = express();

app.engine('mustache', mustacheExpress());
app.set('views', './views');
app.set('view engine,', 'mustache');
app.use(express.static('./public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extend: false}));

app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true
}));

app.use(function (req, res, next) {
    var views = req.session.views;

    if (!views) {
        views = req.session.views = {};
    }

    var pathname = (views[pathname] || 0) + 1;

    next();
})

function authenticate(req, username, password) {
    var authenticatedUser = data.users.find(function (user) {
        if (username === user.username && password === user.password){
            req.session.authenticated = true;
            console.log('User and password Authenticated');
        } else {
            return false
        }

    });

    console.log(req.session);
    return req.session;
}