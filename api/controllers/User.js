'use strict';

var mongoose = require('mongoose'),
    User = mongoose.model('User');

exports.create = function (req, res) {
    var new_user = new User(req.body);
    console.log(req);
    new_user.save(function (err, user) {
        if (err) {
            res.status(400)
            res.send(err);
        }
        res.json(user)
    });
};

exports.find = function (req, res) {
    User.findById(req.params.id, function (err, user) {
        if (err) {
            res.status(400)
            res.send(err);
        }
        res.json(user);
    });
};

exports.update = function (req, res) {
    User.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, function (err, user) {
        if (err) {
            res.status(400)
            res.send(err);
        }
        res.json(user);
    });
};

exports.delete = function (req, res) {

    User.remove({
        _id: req.params.id
    }, function (err, user) {
        if (err) {
            res.status(400)
            res.send(err);
        }
        res.json({message: 'User successfully deleted'});
    });

};