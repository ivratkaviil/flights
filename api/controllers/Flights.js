'use strict';

var mongoose = require('mongoose'),
    Flight = mongoose.model('Flight');

exports.all = function (req, res) {
    Flight.find({}, function (err, flights) {
        if (err)
            res.send(err);
        res.json(flights);
    });
};

exports.create = function (req, res) {
    var new_flight = new Flight(req.body);
    console.log(req);
    new_flight.save(function (err, flight) {
        if (err) {
            res.status(400)
            res.send(err);
        }
        res.json(flight)
    });
};

exports.find = function (req, res) {
    Flight.findById(req.params.id, function (err, flight) {
        if (err) {
            res.status(400)
            res.send(err);
        }
        res.json(flight);
    });
};

exports.update = function (req, res) {
    Flight.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, function (err, flight) {
        if (err) {
            res.status(400)
            res.send(err);
        }
        res.json(flight);
    });
};

exports.delete = function (req, res) {

    Flight.remove({
        _id: req.params.id
    }, function (err, flight) {
        if (err) {
            res.status(400)
            res.send(err);
        }
        res.json({message: 'Flight successfully deleted'});
    });

};